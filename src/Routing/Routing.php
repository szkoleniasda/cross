<?php

namespace Sda\Cross\Routing;

/**
 * Class Routing
 * @package Sda\Cross\Routing
 */
class Routing
{
    const LIGHT = 'light';
    const ALL_LIGHTS = 'lights';
}