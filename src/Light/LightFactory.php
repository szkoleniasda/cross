<?php

namespace Sda\Cross\Light;

/**
 * Class LightFactory
 * @package Sda\Cross\Light
 */
class LightFactory
{

    /**
     * @param $id
     * @param array $param
     * @return Light
     * @throws LigthFactoryException
     */
    public static function makeWithId($id, array $param)
    {
        if (false === array_key_exists('state', $param)
        ){
            throw new LigthFactoryException('Incorrect light params');
        }

        $builder = new LightBuilder();

        return $builder
            ->withId($id)
            ->withState($param['state'])
            ->build();
    }

    /**
     * @param array $param
     * @return Light
     * @throws LigthFactoryException
     */
    public static function makeFromArray(array $param)
    {
        if (false === array_key_exists('id', $param) ||
            false === array_key_exists('state', $param)
        ){
            throw new LigthFactoryException('Incorrect light params');
        }

        $builder = new LightBuilder();

        return $builder
            ->withId($param['id'])
            ->withState($param['state'])
            ->build();
    }
}