<?php

namespace Sda\Cross\Light;

/**
 * Class LigthFactoryException
 * @package Sda\Cross\Light
 */
class LigthFactoryException extends \Exception
{
}