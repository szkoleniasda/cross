<?php

namespace Sda\Cross\Light;

/**
 * Class LightNotFoundException
 * @package Sda\Cross\Light
 */
class LightNotFoundException extends \Exception
{
}