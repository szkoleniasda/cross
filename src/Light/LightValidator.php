<?php

namespace Sda\Cross\Light;

/**
 * Class LightValidator
 * @package Sda\Cross\Light
 */
class LightValidator
{
    /**
     * @param Light $light
     */
    public function validateLight(Light $light)
    {
        return in_array($light->getState(), Light::$availableStates, true);
    }
}