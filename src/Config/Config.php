<?php

namespace Sda\Cross\Config;

class Config
{
    public static $connectionParams = array(
        'dbname' => 'cross',
        'user' => 'root',
        'password' => '',
        'host' => 'localhost',
        'driver' => 'pdo_mysql',
        'charset' => 'utf8'
    );
}