<?php

use Sda\Cross\Config\Config;
use Sda\Cross\Controller\ApiController;
use Sda\Cross\Db\DbConnection;
use Sda\Cross\Request\Request;
use Sda\Cross\Response\Response;

require_once(__DIR__ . '/../vendor/autoload.php');

$api = new ApiController(
    new Request(),
    new Response(),
    new DbConnection(
        Config::$connectionParams
    )
);

$api->run();