<?php

namespace Sda\Cross\Controller;

use Sda\Cross\Db\DbConnection;
use Sda\Cross\Light\LightFactory;
use Sda\Cross\Light\LightNotFoundException;
use Sda\Cross\Light\LightRepository;
use Sda\Cross\Light\LigthFactoryException;
use Sda\Cross\Request\Request;
use Sda\Cross\Response\Response;
use Sda\Cross\Routing\Routing;

/**
 * Class ApiController
 * @package Sda\Cross\Controller
 */
class ApiController
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Response
     */
    private $response;
    /**
     * @var LightRepository
     */
    private $lightRepository;

    /**
     * ApiController constructor.
     * @param Request $request
     * @param Response $response
     * @param DbConnection $connection
     */
    public function __construct(
        Request $request,
        Response $response,
        DbConnection $connection
    )
    {
        $this->request = $request;
        $this->response = $response;

        $this->lightRepository = new LightRepository($connection);
    }

    public function run()
    {
        $action = $this->request->getParamFormGet('action');

        switch ($action){
            case Routing::LIGHT:
                $id = $this->request->getParamFormGet('id', 0);

                if (0 === $id) {
                    $this->response->send404();
                }

                if(Request::HTTP_METHOD_GET === $this->request->getHttpMethod() ){
                    $this->getLight($id);
                }else if(Request::HTTP_METHOD_POST === $this->request->getHttpMethod() ){



                    $this->setLight($id);
                }else{
                    $this->response->send('', Response::STATUS_400_BAD_REQUEST);
                }

                break;
            case Routing::ALL_LIGHTS:
                $this->response->send('[{"id":"1", "state":"yellow"}, {"id":"2", "state":"red"},{"id":"3", "state":"yellow"}, {"id":"4", "state":"red"}]');
                break;
            default:
                $this->response->send404();
                break;
        }
    }

    private function getLight($id)
    {
        try {
            $light = $this->lightRepository->getLight($id);
            $this->response->send($light);
        } catch (LightNotFoundException $e) {
            $this->response->send404();
        }
    }

    private function setLight($id)
    {
        $inputData =  $this->request->getJsonRequestBody();

        try {
            $light = LightFactory::makeWithId($id, $inputData);

            if (false === $light->validate()){
                $this->response->send('Incorrect state', Response::STATUS_400_BAD_REQUEST);
            }

            $this->lightRepository->save($light);
        }catch (LigthFactoryException $e){
            $this->response->send('Undefined state param in request', Response::STATUS_400_BAD_REQUEST);
        }
    }

}